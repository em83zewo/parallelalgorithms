public interface NodeDesignator {
    public String designate(TreeNode treeNode);
}
