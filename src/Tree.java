import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Tree {

    private TreeNode root;
    private List<List<TreeNode>> layers;

    public Tree(TreeNode root) {
        this.root = root;
        this.layers = new ArrayList<>();
        computeDepths(root);
        computeLeafCounts(root);
    }

    public TreeNode getRoot() {
        return root;
    }

    public void calculateIds() {
        for (TreeNode leaf : layers.get(0)) {
            leaf.setId(1);
        }
        AtomicInteger lastID = new AtomicInteger(1); // use AtomicInteger for thread safety
        for (int depth = 1; depth < layers.size(); depth++) {
            // sort nodes in layer by composite ID
            layers.get(depth).sort((t1, t2) -> compareIntArray(t1.getCompositeId(), t2.getCompositeId()));
            // give first node in order its ID
            layers.get(depth).get(0).setId(lastID.incrementAndGet());
            layers.get(depth).get(0).sortChildrenById();
            // iterate other nodes by order, increment ID only if compositeID is higher than before
            for (int i = 1; i < layers.get(depth).size(); i++) {
                int compareToPrevious = compareIntArray(layers.get(depth).get(i - 1).getCompositeId(), layers.get(depth).get(i).getCompositeId());
                if (compareToPrevious < 0) {
                    lastID.incrementAndGet();
                } else if (compareToPrevious > 0) {
                    throw new IllegalStateException("Nodes in layer were not iterated in correct order.");
                }
                layers.get(depth).get(i).setId(lastID.get());
                layers.get(depth).get(i).sortChildrenById();
            }
        }
    }

    public List<List<Tree>> computePFDs(TreeNode node) {
        List<List<Tree>> PFDs = new ArrayList<>();
        int n = node.getLeafCount();
        List<Integer> factorSizes = new ArrayList<>();
        kLoop : for (int k = n / 2; k >= 2; k--) {
            // k is the size of the right factor
            int finalK = k;
            if (n % k != 0 || factorSizes.stream().anyMatch(f -> finalK % f == 0))
                continue;
            List<TreeNode> RP = new ArrayList<>();  // set R and P
            // get candidate vertices -> vertices with at least one child with leafCount(child) < k, and all candidates having the same label
            if (!getRP(node, RP, k))
                continue;
            // filter the candidates
            int rightFactorRootLabel = -1;
            List<TreeNode> rightFactorSecondLevelVertices = new ArrayList<>();
            uLoop : for (TreeNode u : RP) {
                List<TreeNode> children = u.getChildren().stream().filter(child -> child.getLeafCount() < finalK)
                        .sorted(Comparator.comparingInt(TreeNode::getId)).collect(Collectors.toList());
                int size = children.stream().mapToInt(TreeNode::getLeafCount).sum();
                if (!(u.getLeafCount() % k == 0))
                    continue kLoop;
                int noOfCopies = size/k;
                if (children.size() % noOfCopies == 0)
                    continue kLoop;
                if (rightFactorRootLabel == -1) {
                    rightFactorRootLabel = u.getLabel();
                } else if (rightFactorRootLabel != u.getLabel()) {
                    continue kLoop;
                }
                for (int i = 0; i < noOfCopies; i++) {
                    if (rightFactorSecondLevelVertices.isEmpty()) {
                        for (int j = 0; j < children.size() / noOfCopies; j++) {
                            rightFactorSecondLevelVertices.add(children.get(i * noOfCopies + i));
                        }
                    } else {
                        for (int j = 0; j < children.size() / noOfCopies; j++) {
                            if (rightFactorSecondLevelVertices.get(j).getId() != children.get(j * noOfCopies + i).getId()) {
                                continue kLoop;
                            }
                        }
                    }
                }
                factorSizes.add(k);
                Tree leftFactor = getLeftFactor(RP, k);
                Tree rightFactor = getRightFactor(rightFactorRootLabel, rightFactorSecondLevelVertices);
                List<List<Tree>> rightPFDs = computePFDs(rightFactor.root);
                for (List<Tree> rightPFD : rightPFDs) {
                    List<Tree> newPFD = new ArrayList<>();
                    newPFD.add(leftFactor);
                    newPFD.addAll(rightPFD);
                    PFDs.add(newPFD);
                }
            }
        }
        return PFDs;
    }

    private Tree getLeftFactor(List<TreeNode> vertexList, int k) {
        Tree copy = getSubTreeCopy(root);
        for (TreeNode u : vertexList) {
            List<TreeNode> children = u.getChildren().stream().filter(child -> child.getLeafCount() < k)
                    .sorted(Comparator.comparingInt(TreeNode::getId)).collect(Collectors.toList());
            int size = children.stream().mapToInt(TreeNode::getLeafCount).sum();
            int noOfCopies = size / k;
            for (TreeNode v : children) {
                copy.removeSubTree(v);
            }
            // add noOfCopies many leaves as children of u
            for (int i = 0; i < noOfCopies; i++) {
                u.addChild(new TreeNode());
            }
        }
        return copy;
    }

    private Tree getRightFactor(int rootLabel, List<TreeNode> vertexList) {
        // TODO
        return null;
    }

    public void removeSubTree(TreeNode subTreeRoot) {
        removeSubTreeHelper(subTreeRoot, this.root);
        computeDepths(root);
        computeLeafCounts(root);
        calculateIds();
    }

    private void removeSubTreeHelper(TreeNode subTreeRoot, TreeNode currentNode) {
        for (TreeNode child : currentNode.getChildren()) {
            List<TreeNode> toRemove = new ArrayList<>();
            if (child.getId() == subTreeRoot.getId()) {
                toRemove.add(child);
            } else {
                removeSubTreeHelper(subTreeRoot, child);
            }
            subTreeRoot.getChildren().removeAll(toRemove);
        }
    }

    private Tree getSubTreeCopy(TreeNode root) {
        TreeNode rootCopy = new TreeNode(root.getLabel());
        copyChildrenRecursively(root, rootCopy);
        return new Tree(rootCopy);
    }

    private void copyChildrenRecursively(TreeNode root, TreeNode rootCopy) {
        for (TreeNode child : root.getChildren()) {
            TreeNode childCopy = new TreeNode(child.getLabel());
            rootCopy.addChild(childCopy);
            copyChildrenRecursively(child, childCopy);
        }
    }

    private boolean getRP(TreeNode node, List<TreeNode> RP, int k) {
        for (TreeNode child : node.getChildren()) {
            if (child.getLeafCount() < k) {
                if (RP.stream().allMatch(c -> c.getLabel() == node.getLabel())) {
                    RP.add(node);
                    return true;
                }
                return false;
            } else {
                if (!getRP(child, RP, k)) {
                    return false;
                }
            }
        }
        return true;
    }

    public int computeLeafCounts(TreeNode node) {
        if (node.getChildren().isEmpty()) {
            node.setLeafCount(1);
            return 1;
        }
        int leafCount = 0;
        for (TreeNode child : node.getChildren()) {
            leafCount += computeLeafCounts(child);
        }
        node.setLeafCount(leafCount);
        return leafCount;
    }

    public int computeDepths(TreeNode vertex) {
        int depth = -1;
        if (vertex.getChildren().isEmpty()) {
            depth = 0;
        }
        else {
            for (TreeNode child : vertex.getChildren()) {
                depth = Math.max(depth,computeDepths(child));
            }
            depth ++;
        }
        if (layers.size()<depth+1) {
            layers.add(new ArrayList<TreeNode>());
        }
        vertex.setDepth(depth);
        layers.get(depth).add(vertex);
        return depth;
    }

    public int compareIntArray(int[] a, int[] b) {
        if (a.length != b.length) return a.length - b.length;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) return a[i] - b[i];
        }
        return 0;
    }

    public String getStringRepresentation(NodeDesignator nodeDesignator) {
        return root.getStringRepresentation(nodeDesignator);
    }
}
