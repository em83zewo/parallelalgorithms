import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FileHandler {

    private String filePath;

    public FileHandler(String filePath) {
        this.filePath = filePath;
    }

    public InputGraph parseInputGraph() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        int vertexCount = parseFileHeader();
        InputGraph inputGraph = new InputGraph(vertexCount);
        String line;
        int x = 0;
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) continue;
            int y = 0;
            for (char c : line.replaceAll("\\s+", "").toCharArray()) {
                if (c != '0' && c != '1') {
                    throw new IllegalArgumentException("Bad character '" + c + "' encountered at position " + x + ", " + y + ".");
                }
                inputGraph.setValue(x, y, c == '1');
                y++;
            }
            x++;
        }
        return inputGraph;
    }

    public String getFileContent() {
        StringBuilder contentSB = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(contentSB::append);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentSB.toString();
    }

    public int parseFileHeader() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) {
                return Integer.parseInt(line.substring(1));
            }
        }
        throw new IllegalArgumentException("File Header missing");
    }
}
