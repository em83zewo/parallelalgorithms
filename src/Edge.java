import java.util.Objects;

public class Edge {

    private int vertexA;
    private int vertexB;

    public Edge(int vertexA, int vertexB) {
        this.vertexA = vertexA;
        this.vertexB = vertexB;
    }

    public int getVertexA() {
        return vertexA;
    }

    public void setVertexA(int vertexA) {
        this.vertexA = vertexA;
    }

    public int getVertexB() {
        return vertexB;
    }

    public void setVertexB(int vertexB) {
        this.vertexB = vertexB;
    }

    public Edge getInverse() {
        return new Edge(vertexB, vertexA);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Edge) {
            return (this.vertexA == ((Edge) other).vertexA
                    && this.vertexB == ((Edge) other).vertexB)
                    || (this.vertexA == ((Edge) other).vertexB
                    && this.vertexB == ((Edge) other).vertexA);
        } else {
            return super.equals(other);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(vertexA, vertexB) + Objects.hash(vertexB, vertexA);
    }
}
