import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InputGraph {

    private int vertexCount;
    private boolean[][] adjacent;

    public InputGraph (int vertexCount) {
        this.vertexCount = vertexCount;
        adjacent = new boolean[vertexCount][vertexCount];
    }

    public void setVertexCount(int value) {
        vertexCount = value;
    }

    public void setValue(int x, int y, boolean value) {
        adjacent[x][y] = value;
    }

    public boolean getValue(int x, int y) {
        return adjacent[x][y];
    }

    public Graph getGraph() {
        Graph graph = new Graph();
        for (int i = 0; i < vertexCount; i++) {
            graph.addVertex(i);
        }
        for (int x = 0; x < adjacent.length; x++) {
            for (int y = 0; y < adjacent[x].length; y++) {
                if (adjacent[x][y]) graph.addEdge(new Edge(x, y));
            }
        }
        return graph;
    }

    public Set<List<Integer>> getConnectedComponents() {
        Set<List<Integer>> connectedComponents = new HashSet<>();
        for (int x = 0; x < adjacent.length; x++) {
            createNewComponentIfNotExists(connectedComponents, x);
            addAllNeighborsToComponent(connectedComponents, x);
        }
        return connectedComponents;
    }

    public Set<InputGraph> getConnectedSubGraphs() {
        Set<InputGraph> connectedSubGraphs = new HashSet<>();
        Set<List<Integer>> connectedComponents = getConnectedComponents();
        for (List<Integer> component : connectedComponents) {
            InputGraph currentSubGraph = new InputGraph(vertexCount);
            for (Integer x : component) {
                for (Integer y: component) {
                    if (x.equals(y)) continue;
                    currentSubGraph.setValue(x, y, getValue(x, y));
                }
            }
            connectedSubGraphs.add(currentSubGraph);
        }
        return connectedSubGraphs;
    }

    public boolean isConnected() {
        return getConnectedComponents().size() == 1;
    }

    public InputGraph getComplement() {
        InputGraph complement = new InputGraph(vertexCount);
        for (int x = 0; x < adjacent.length; x++) {
            for (int y = 0; y < adjacent[x].length; y++) {
                complement.setValue(x, y, !getValue(x, y));
            }
        }
        return complement;
    }

    private void createNewComponentIfNotExists(Set<List<Integer>> connectedComponents, int x) {
        for (List<Integer> currentComponent : connectedComponents) {
            if (currentComponent.contains(x)) {
                return;
            }
        }
        List<Integer> newComponent = new ArrayList<>();
        newComponent.add(x);
        connectedComponents.add(newComponent);
    }

    private void addAllNeighborsToComponent(Set<List<Integer>> connectedComponents, int x) {
        for (int y = 0; y < adjacent[x].length; y++) {
            if (adjacent[x][y]) {
                for (List<Integer> currentComponent : connectedComponents) {
                    if (currentComponent.contains(x) && !currentComponent.contains(y)) {
                        currentComponent.add(y);
                        break;
                    }
                }
            }
        }
    }
}
