#!/bin/bash

$JAVA_VERSION -jar omp4j-1.2.jar -d ../classes -v *.java
cd ../classes || exit
java Main
