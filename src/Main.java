import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
        FileHandler fileHandler = new FileHandler("data/graph");
        InputGraph inputGraph = fileHandler.parseInputGraph();
        Graph g = inputGraph.getGraph();
        System.out.println(g);
        TreeNode root = Graph.getCoTree(g);
        System.out.println(root);

        Tree tree = new Tree(root);
        System.out.println("\nDepths:");
        System.out.println(tree.getStringRepresentation(node -> String.valueOf(node.getDepth())));
        tree.calculateIds();
        System.out.println("\nIDs:");
        System.out.println(tree.getStringRepresentation(node -> String.valueOf(node.getId())));
        System.out.println("\nComposite IDs:");
        System.out.println(tree.getStringRepresentation(node -> Arrays.toString(node.getCompositeId())));
    }
}
