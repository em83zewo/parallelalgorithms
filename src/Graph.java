import java.util.*;
import java.util.stream.Collectors;

/**
 * Undirected Graph
 */
public class Graph {

    private final List<Integer> vertices;
    private final Set<Edge> edges;

    public Graph() {
        this.vertices = new ArrayList<>();
        this.edges = new HashSet<>();
    }

    public List<Integer> getVertices() {
        return vertices;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public boolean areConnected(int a, int b) {
        return edges.stream().anyMatch(e -> e.equals(new Edge(a, b)));
    }

    public void addEdge(Edge e) {
        if (!vertices.contains(e.getVertexA()) || !vertices.contains(e.getVertexB())) {
            throw new IllegalArgumentException("Cannot add edge because specified vertices do not exist: " + e.getVertexA() + ", " + e.getVertexB());
        }
        if (!edges.contains(e)) this.edges.add(e);
    }

    public void addVertex(int v) {
        if (this.vertices.contains(v)) return;
        this.vertices.add(v);
    }

    public Set<Integer> getNeighbors(int vertex) {
        Set<Integer> neighbors = new HashSet<>();
        for (Integer v : vertices) {
            if (areConnected(v, vertex)) {
                neighbors.add(v);
            }
        }
        return neighbors;
    }

    public Graph getComplement() {
        Graph complement = new Graph();
        for (Integer v : vertices) {
            complement.addVertex(v);
            for (Integer w : vertices) {
                complement.addVertex(w);
                if (v.equals(w)) continue;
                if (!areConnected(v, w)) {
                    complement.addEdge(new Edge(v, w));
                }
            }
        }
        return complement;
    }

    public static TreeNode getCoTree(Graph g) {
        if (g.vertices.size() == 1) {
            return new TreeNode(g.vertices.get(0));
        }
        Set<Graph> connectedSubGraphs = g.getConnectedSubGraphs();
        if (connectedSubGraphs.size() == 1) {
            // graph is connected
            return getCoTreeRecursive(g.getComplement(), 1);
        } else {
            return getCoTreeRecursive(g, 0);
        }
    }

    private static TreeNode getCoTreeRecursive(Graph g, int label) {
        TreeNode root = new TreeNode(label);
        Set<Graph> connectedSubGraphs = g.getConnectedSubGraphs();
        Graph[] connectedSubGraphArray = connectedSubGraphs.toArray(new Graph[0]);
        // omp parallel for
        for (int i = 0; i < connectedSubGraphArray.length; i++) {
            Graph subGraph = connectedSubGraphArray[i];
            subGraph = subGraph.getComplement();
            if (subGraph.vertices.size() == 1) {
                root.addChild(new TreeNode(subGraph.vertices.get(0)));
            } else {
                root.addChild(getCoTreeRecursive(subGraph, label == 1 ? 0 : 1));
            }
        }
        root.setLabel(label);
        return root;
    }

    public Set<Graph> getConnectedSubGraphs() {
        Set<Graph> connectedSubGraphs = new HashSet<>();
        for (Integer v : vertices) {
            createNewSubGraphIfNotExists(connectedSubGraphs, v);
        }
        return connectedSubGraphs;
    }

    private void createNewSubGraphIfNotExists (Set<Graph> connectedSubGraphs, int v) {
        for (Graph subGraph : connectedSubGraphs) {
            if (subGraph.vertices.contains(v)) {
                return;
            }
        }
        Graph newSubGraph = new Graph();
        addNeighborsRecursive(newSubGraph, v);
        connectedSubGraphs.add(newSubGraph);
    }

    private void addNeighborsRecursive(Graph g, int v) {
        if (g.vertices.contains(v)) return;
        g.vertices.add(v);
        for (Integer n : getNeighbors(v)) {
            addNeighborsRecursive(g, n);
            g.addEdge(new Edge(n, v));
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Vertices: ");
        for (Integer v : vertices) {
            sb.append(v.toString()).append(", ");
        }
        sb.append("\nEdges: ");
        for (Edge e : edges.stream().sorted((o1, o2) -> {
            int d = Integer.compare(o1.getVertexA(), o2.getVertexA());
            if (d == 0)
                return Integer.compare(o1.getVertexB(), o2.getVertexB());
            else
                return d;
        }).collect(Collectors.toList())) {
            sb.append("\n").append(e.getVertexA()).append("---").append(e.getVertexB());
        }
        return sb.toString();
    }

}
