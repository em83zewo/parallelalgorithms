import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class TreeNode {

    private List<TreeNode> children;
    private int label;
    private int depth;
    private int id;
    private int leafCount;

    public TreeNode() {
        this.children = new ArrayList<>();
    }

    public TreeNode(int label) {
        this.children = new ArrayList<>();
        this.label = label;
    }

    public TreeNode(Collection<TreeNode> children) {
        this.children = new ArrayList<>();
        this.children.addAll(children);
    }

    public void addChild(TreeNode child) {
        this.children.add(child);
    }

    public void addChildren(Collection<TreeNode> children) {
        this.children.addAll(children);
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void sortChildrenById() {
        children.sort((c1, c2) -> Integer.compare(c2.id, c1.id));
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getLeafCount() {
        return leafCount;
    }

    public void setLeafCount(int leafCount) {
        this.leafCount = leafCount;
    }

    @Override
    public String toString() {
        return getStringRepresentation(n -> String.valueOf(n.label));
    }

    public String getStringRepresentation(NodeDesignator nodeDesignator) {
        StringBuilder buffer = new StringBuilder(50);
        print(buffer, "", "", nodeDesignator);
        return buffer.toString();
    }

    public void print(StringBuilder buffer, String prefix, String childrenPrefix, NodeDesignator nodeDesignator) {
        buffer.append(prefix);
        buffer.append(nodeDesignator.designate(this));
        buffer.append('\n');
        for (Iterator<TreeNode> it = children.iterator(); it.hasNext();) {
            TreeNode next = it.next();
            if (it.hasNext()) {
                next.print(buffer, childrenPrefix + "├── ", childrenPrefix + "│   ", nodeDesignator);
            } else {
                next.print(buffer, childrenPrefix + "└── ", childrenPrefix + "    ", nodeDesignator);
            }
        }
    }

    /**
     * All IDs of descending nodes from this must be calculated before calling this method.
     * @return An array representing the composite ID of this node.
     */
    public int[] getCompositeId() {
        sortChildrenById();
        int[] subID = new int[getChildren().size()];
        for (int i = 0; i < getChildren().size(); i++) {
            subID[i] = getChildren().get(i).getId();
        }
        return subID;
    }
}
